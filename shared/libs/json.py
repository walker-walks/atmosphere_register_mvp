import json
import datetime
import bson.objectid

def custom_handler(x):
    if isinstance(x, datetime.datetime):
        return x.isoformat()
    elif isinstance(x, bson.objectid.ObjectId):
        return str(x)
    else:
        raise TypeError(x)

def dump(bson):
    return json.dumps(bson, ensure_ascii=False, default=custom_handler)