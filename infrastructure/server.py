from flask import Flask

server = Flask(__name__)
# limit upload file size : 20MB
server.config['MAX_CONTENT_LENGTH'] = 20 * 1024 * 1024
route = server.route
