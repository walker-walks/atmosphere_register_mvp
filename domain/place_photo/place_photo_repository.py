import os
import config
from datetime import datetime
from flask import send_from_directory
from werkzeug.utils import secure_filename
from domain.place_photo import playce_photo_contant

def allowed_file(filename):
  return '.' in filename and \
    filename.rsplit('.', 1)[1] in config.ALLOWED_IMG_EXTENSIONS


def save(img_file):
  complete_filename = secure_filename(img_file.filename)
  _, extension = os.path.splitext(complete_filename)
  new_filename = datetime.now().strftime('%Y_%m_%d_%S') + extension
  img_file.save(os.path.join(config.UPLOAD_DIR, new_filename))
  img_url = playce_photo_contant.URI + '/' + new_filename
  return img_url

def get(file_name):
  return send_from_directory(config.UPLOAD_DIR, file_name)