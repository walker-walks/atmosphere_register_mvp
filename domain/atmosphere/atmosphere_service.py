import http.client, urllib.request, urllib.parse, urllib.error, base64

def recognize(image_file):
  headers = {
  'Content-Type': 'application/octet-stream',
  'Ocp-Apim-Subscription-Key': '6551891f2756415e93feb6c15fc07cb0',
  }

  api_endpoint = '/face/v1.0/detect?returnFaceId=true&returnFaceLandmarks=false&returnFaceAttributes=emotion'

  try:
    conn = http.client.HTTPSConnection('brazilsouth.api.cognitive.microsoft.com')
    conn.request("POST", api_endpoint, headers=headers, body=image_file)
    response = conn.getresponse()
    data = response.read()
    conn.close()
    return data
  except Exception as e:
    print("[Errno] {0}".format(e))
