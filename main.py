import infrastructure.router
from infrastructure.server import server

def main():
  server.run(host='127.0.0.1', port=8080)

if __name__ == '__main__':
  main()