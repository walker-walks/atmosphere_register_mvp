import os

UPLOAD_DIR = os.path.dirname(os.path.abspath(__file__)) + '/uploads'
HOST_NAME = os.getenv("UPLOAD_DIR_PATH") if os.getenv("UPLOAD_DIR_PATH") != None else 'localhost'
print( 'host_name: ' + HOST_NAME)

ALLOWED_IMG_EXTENSIONS = set(['png', 'jpg'])