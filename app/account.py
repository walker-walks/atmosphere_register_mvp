from infrastructure.server import route
from domain.account.account_repository import account_repository
from shared.libs.json import dump

@route('/account')
def findAll(accountRepo = account_repository):
    res = accountRepo.find_one()
    return dump(res)
