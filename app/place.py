# -*- coding: utf-8 -*-
from infrastructure.server import route
from domain.atmosphere import atmosphere_service
from shared.libs.json import dump
from flask import Flask, request, make_response, jsonify

@route('/place/atmosphere-register', methods=['POST'])
def createAtmosphereRegister(atmosphereRepo = atmosphere_service):
  if 'image_file' not in request.files:
    return {'result':'image_file is required.'}
  image_file = request.files['image_file']

  result = atmosphereRepo.recognize(image_file)

  return result
